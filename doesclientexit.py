# Importing Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Importing Dataset
dataset = pd.read_csv("Churn_Modelling.csv")
# Checking Null Values
dataset.isnull().sum()
# We Find 0 Null Values
X = dataset.iloc[:, 3:-1].values
y = dataset.iloc[:, 13].values

# Categorical Variables
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_Geography = LabelEncoder()
X[:, 1] = labelencoder_Geography.fit_transform(X[:, 1])
labelencoder_Gender = LabelEncoder()
X[:, 2] = labelencoder_Gender.fit_transform(X[:, 2])
onehotencoder = OneHotEncoder(categorical_features = [1])
X = onehotencoder.fit_transform(X).toarray()
X = X[:, 1:]

# Splitting Train And Test Set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

# Dimensionality Reduction
from sklearn.decomposition import PCA
pca = PCA(n_components = 3)
X_train = pca.fit_transform(X_train)
X_test = pca.transform(X_test)
explained_variance = pca.explained_variance_ratio_

# Selecting the Best SVC Params
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
parameters = [{'C': [1, 10, 100, 1000], 'kernel': ['linear']},
              {'C': [1, 10, 100, 1000], 'kernel': ['rbf'], 'gamma': [0.5, 0.1, 0.01, 0.001, 0.0001]}]
grid_search = GridSearchCV(estimator = SVC(),
                           param_grid = parameters,
                           scoring = 'accuracy',
                           cv = 10,
                           n_jobs = -1)
grid_search = grid_search.fit(X_train, y_train)
best_accuracy = grid_search.best_score_
best_parameters = grid_search.best_params_

# We Get C = 1000, Kernal = RBF, Gaama = 0.5 as the best Params
# Putting the Params and Creating Classifier
classifier = SVC(C = 1000, kernel = 'rbf', gamma = 0.5)
classifier.fit(X_train, y_train)

# Predicting the Test Set
y_pred = classifier.predict(X_test)

# Creating the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_pred, y_test)

# K Folds Cross Validation
from sklearn.model_selection import cross_val_score
accuracies = cross_val_score(estimator = classifier, X = X_train, y = y_train, cv = 10)
accuracies.mean()
accuracies.std()

# Visualizing the Age Groups That Closed Bank Accounts the Most
plt.title("Age Group That Closed Their Accounts The Most")
sns.countplot(x = dataset[dataset.Exited == 1].Age, order = dataset[dataset.Exited == 1].Age.value_counts().index[0:5])

# Visualizing The Gender Which Exited The Most
plt.title("Gender Which Exited The most")
sns.countplot(x = dataset[dataset.Exited == 1].Gender, order = dataset[dataset.Exited == 1].Gender.value_counts().index[0:2])

# Location From Which The People Who Exited The Most Reside In
plt.title("Location From Which The People Who Exited The Most Reside In")
sns.countplot(x = dataset[dataset.Exited == 1].Geography, order = dataset[dataset.Exited == 1].Geography.value_counts().index[0:3])
